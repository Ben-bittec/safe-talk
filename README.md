# safe-talk

## Safe-Talk the new Open-Source End to End Communication App

### App Version Definitive Framework: React/Firebase
(Firebase is used only to build the App, until we have a stable Server Owned by the Project)

Sources:[Tutorial](https://www.youtube.com/watch?v=Q-y8ASwOYgQ)

Easiest way to run the Application for Developing:

### Static Server

For environments using Node, the easiest way to handle this would be to install serve and let it handle the rest:

npm install -g serve

serve -s build

The last command shown above will serve your static site on the port 5000. Like many of serve’s internal settings, the port can be adjusted using the -l or --listen flags:

serve -s build -l (port)

Run this command to get a full list of the options available: serve -h

### First Steps to take:

1. Develop a Unique UI 
2. Substitute the actual login method with a PGP Based QR Identification by Cam (Smartphones,Tables, etc)
3. Creating a safe direct P2P or maybe a serverbased SSH Client to client Only Connection (PGP/QR based fingerprint/direct conv.) 
4. Safe your Conversations Option?

Contact for Interested Co-Nerds: info@bittec.ch
