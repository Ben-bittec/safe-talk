import firebase from 'firebase'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyDMdNL0RbC50kYcd0BT6vCtoFovV31yT2A",
    authDomain: "safe-talk-d3a31.firebaseapp.com",
    projectId: "safe-talk-d3a31",
    storageBucket: "safe-talk-d3a31.appspot.com",
    messagingSenderId: "461570478033",
    appId: "1:461570478033:web:df1950f460c43fc329daf0",
    measurementId: "G-HX5Y56G1ES"
})

const db = firebaseApp.firestore()

const auth = firebase.auth()

export { db, auth }